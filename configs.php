<?php

$CONFIGS['variableScssFile'] = "_variables.scss";
$CONFIGS['font'] = 'fontawesome-webfont.ttf';

$CONFIGS['iconsDataFile'] = 'icons.data.php';
$CONFIGS['outputDir'] = '.\\fa-icons\\';
$CONFIGS['outputDirUrl'] = '/fa2png/fa-icons';

$CONFIGS['outputBase64'] = true;

// Define la API en funci�n del hostname
$hostname = strtolower(gethostname());
$lander = "/lander/i";
if (preg_match($lander, $hostname))
	$CONFIGS['SERVER'] = "http://apiweb.icemd.dev";
else
	$CONFIGS['SERVER'] = "http://apiweb.icemd.com";

